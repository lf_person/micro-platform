package com.dj.scgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class SpringCloudGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudGatewayApplication.class, args);
    }

    /**
     * 创建路由器规则
     * @param builder 路由构造器
     * @return
     */
//    @Bean
//    public RouteLocator customRouteLocator(RouteLocatorBuilder builder){
//        return builder.routes()  //开启路由配置
//        //匹配路径
//        .route(f->f.path("/user/**")  //route方法逻辑是一个断言
//                //转发到具体的url
//                .uri("lb://user-center"))  //转发到具体的url
//                .build(); //创建
//    }

}
